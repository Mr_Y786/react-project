import React from 'react';
import { Form, Input, Button, message } from 'antd';
import { UserOutlined, LockOutlined,ExclamationCircleOutlined } from '@ant-design/icons';
import { http } from "../../http";
import qs from 'qs';
import "./login.css"

class Login extends React.Component {
    
    onFinish = values => {
        http("manage/user/login.do", "POST", qs.stringify(values)).then(res => {
            if (res.status === 0) {
                message.success({
                    content:res.msg,
                    duration:3,
                    icon:<ExclamationCircleOutlined />
                });
                this.props.history.push('/home/index')
            }
        })
        // console.log('Received values of form: ', values);
    };
    render() {
        return (

            <div className="login">
                
                <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={this.onFinish}
                >
                    <Form.Item
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: '请输入用户名!',
                            },
                        ]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="用户名" />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: '请输入密码!',
                            },
                        ]}
                    >
                        <Input
                            prefix={<LockOutlined className="site-form-item-icon" />}
                            type="password"
                            placeholder="密码"
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            登陆
            </Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }
}

export { Login }