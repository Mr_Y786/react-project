import React from 'react';
import { Breadcrumb, Button, Table ,message} from 'antd';
import { http } from "../../http";
const { Column } = Table;
export class Order extends React.Component{
    state = {
        data: [],
        params: {
            pageNum: 1,
            pageSize: 10
        },
        value: ""
    }
    getList = (params) => {
        http("manage/order/list.do", "GET", {}, params).then(res => {
            this.setState({
                data: res.data.list,
                pagination: {
                    ...this.state.pagination,
                    total: res.data.total
                }
            })
        })
    }
    //分页请求数据
    changePage = (page) => {
        const { current, pageSize } = page
        let params = {
            pageNum: current,
            pageSize
        }
        if (this.state.value) {
            // this.search(params)
        } else {
            this.getList(params);
        }


    }
    //input值
    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }
    //input清空
    reset = () => {
        this.setState({
            value: ""
        })
        this.getList(this.state.params);
    }
    //查询
    search = () => (e) => {
        http("manage/order/search.do", "GET", {}, {orderNo:this.state.value}).then(res => {
            if(res.status===1){
                message.error({
                    content:res.msg,
                    duration:3,
                    icon:""
                });
            }else{
                this.setState({
                    data: res.data.list,
                    pagination: {
                        ...this.state.pagination,
                        total: res.data.total
                    }
                })
            }
            
        })
    }
    //跳转到详情页
    toInfo=(obj)=>(e)=>{
        this.props.history.push('/home/orderinfo/'+obj.orderNo)
    }
    render() {
        return (
            <div>
                {/* 面包屑 */}
                <div className="brand">
                    <Breadcrumb>
                        <Breadcrumb.Item>首页</Breadcrumb.Item>
                        <Breadcrumb.Item>订单 </Breadcrumb.Item>
                        <Breadcrumb.Item>订单管理</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="brand_head">订单管理</div>
                </div>
                <div className="table_list">
                    <div className="table_inp">
                        <div className="inp">关键词： <input type="text" value={this.state.value} onChange={this.handleChange} /></div>
                        <div className="btn">
                            <Button type="primary" onClick={this.search(this.state.params)}>搜索</Button>
                            <Button onClick={this.reset}>清空</Button>
                        </div>
                    </div>
                    <Table dataSource={this.state.data} rowKey={record => record.orderNo} pagination={this.state.pagination} onChange={this.changePage}>
                        <Column title="订单号" dataIndex="orderNo" key="orderNo" />
                        <Column title="收件人" dataIndex="receiverName" key="receiverName" />
                        <Column title="订单状态" dataIndex="statusDesc" key="statusDesc"
                           
                        />
                        <Column
                            title="订单总价"
                            dataIndex="status"
                            key="status"
                            render={(text) => (
                                <span>￥ {text}</span>
                            )}
                        />
                        <Column
                            title="创建时间"
                            dataIndex="createTime"
                            key="createTime"
                        />
                        <Column
                            title="操作"
                            key="action"
                            render={(text, record) => (
                                <span>
                                    <a style={{ marginRight: 8 }} onClick={this.toInfo(record)}>查看详情</a>
                                </span>
                            )}
                        />
                    </Table>
                </div>

            </div>
        )
    }
    componentDidMount() {
        this.getList(this.state.params);
    }
}