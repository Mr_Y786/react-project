import React from 'react';
import { Breadcrumb, PageHeader, Descriptions, Table } from 'antd';
import "../shop/shopinfo.css"
import { http } from "../../http"
const { Column } = Table;
export class Orderinfo extends React.Component {
    state = {
        orderNo: "",
        infoList: {},
        pagination:{
            hideOnSinglePage:true
        }
    }
    getInfo() {
        http("manage/order/detail.do", "GET", {}, {
            orderNo: this.state.orderNo
        }).then(res => {
            this.setState({
                infoList: res.data
            })
        })
    }
    render() {
        return (
            <div className="shopinfo">
                <div className="brand">
                    <Breadcrumb>
                        <Breadcrumb.Item>首页</Breadcrumb.Item>
                        <Breadcrumb.Item>订单 </Breadcrumb.Item>
                        <Breadcrumb.Item>订单管理</Breadcrumb.Item>
                        <Breadcrumb.Item>订单详情</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="brand_head">订单详情</div>
                </div>
                <div className="info_main">
                    <PageHeader
                        ghost={false}
                        subTitle="订单基本信息"
                    >
                        <Descriptions size="small" column={3}>
                            <Descriptions.Item label="订单号">{this.state.infoList.orderNo}</Descriptions.Item>
                            <Descriptions.Item label="创建时间">
                                {this.state.infoList.createTime}
                            </Descriptions.Item>
                            <Descriptions.Item label="支付状态">{this.state.infoList.statusDesc}</Descriptions.Item>
                            <Descriptions.Item label="支付方式">{this.state.infoList.paymentTypeDesc}</Descriptions.Item>
                            <Descriptions.Item label="订单金额">￥{this.state.infoList.payment}</Descriptions.Item>
                        </Descriptions>
                    </PageHeader>
                    <PageHeader
                        ghost={false}
                        subTitle="收件人信息"
                        className="info_des"
                    >
                        <Descriptions>
                            <Descriptions.Item label="收件人">{this.state.infoList.receiverName}</Descriptions.Item>
                            <Descriptions.Item label="联系电话">
                            {this.state.infoList.receiverName}
                                111
                            </Descriptions.Item>
                            <Descriptions.Item label="收货地址">
                            {this.state.infoList.receiverName}222
                            </Descriptions.Item>
                            <Descriptions.Item label="邮政编码">
                               {this.state.infoList.receiverName}33
                            </Descriptions.Item>
                        </Descriptions>
                    </PageHeader>
                    <PageHeader
                        ghost={false}
                        subTitle="商品信息"
                        className="info_des"
                    >
                        <Table dataSource={this.state.infoList.orderItemVoList} rowKey={record => record.orderNo} className="table_info" pagination={this.state.pagination}>
                            <Column title="图片" dataIndex="productImage" key="productImage" render={(text, record) => (
                                <img src={this.state.infoList.imageHost + record.productImage} className="info_img" />
                            )} />
                            <Column title="名称" dataIndex="productName" key="productName" />
                            <Column title="单价" dataIndex="currentUnitPrice" key="currentUnitPrice" />
                            <Column title="数量" dataIndex="quantity" key="quantity" />
                            <Column title="总价" dataIndex="totalPrice" key="totalPrice" />

                        </Table>
                    </PageHeader>

                </div>
            </div >
        )
    }
    async componentDidMount() {
        await this.setState({
            orderNo: this.props.match.params.orderNo
        })
        this.getInfo()
    }
}