import React from 'react';
import "./shopedit.css"
import {http} from "../../http"
import { Form, Input, InputNumber, Button, Select, Switch, Upload, Modal, Breadcrumb } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'
//图片上传
function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}
//表格设置
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const validateMessages = {
  required: '此为必填项',

};
//定义富文本
class EditorDemo extends React.Component {
  state = {
    // 创建一个空的editorState作为初始值
    editorState: BraftEditor.createEditorState(null)
  }

  async componentDidMount() {
    // 假设此处从服务端获取html格式的编辑器内容
    const htmlContent = 'Hello World!'
    // 使用BraftEditor.createEditorState将html字符串转换为编辑器需要的editorStat
    this.setState({
      editorState: BraftEditor.createEditorState(htmlContent)
    })
  }

  submitContent = async () => {
    // 在编辑器获得焦点时按下ctrl+s会执行此方法
    // 编辑器内容提交到服务端之前，可直接调用editorState.toHTML()来获取HTML格式的内容
    // const htmlContent = this.state.editorState.toHTML()
    // const result = await saveEditorContent(htmlContent)
  }

  handleEditorChange = (editorState) => {
    this.setState({ editorState })
  }

  render() {
    const { editorState } = this.state
    return (
      <div className="my-component">
        <BraftEditor
          value={editorState}
          onChange={this.handleEditorChange}
          onSave={this.submitContent}
        />
      </div>
    )
  }
}
export class ShopEdit extends React.Component {
  state = {
    previewVisible: false,
    previewImage: '',
    fileList: [],
    productId:"",
    list:{}
  };
  //获取信息
  getList(){
    http("manage/product/detail.do","GET",{},{
      productId:this.state.productId
    }).then(res=>{
     this.setState({
      list:res.data
     })
    })
  }
  //上传设置
  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
    });
  };

  handleChange = ({ fileList }) => this.setState({ fileList });
  //表单提交
  onFinish = values => {
    console.log(values);
  };
  render() {
    const { previewVisible, previewImage, fileList } = this.state;
    const uploadButton = (
      <div>
        <PlusOutlined />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    return (
      <div>
        <div className="brand">
          <Breadcrumb>
            <Breadcrumb.Item>首页</Breadcrumb.Item>
            <Breadcrumb.Item>商品 </Breadcrumb.Item>
            <Breadcrumb.Item>商品列表</Breadcrumb.Item>
            <Breadcrumb.Item>新增商品</Breadcrumb.Item>
          </Breadcrumb>
          <div className="brand_head">新增商品</div>
        </div>
        <div className="edit">

          <Form {...layout} name="nest-messages" onFinish={this.onFinish} validateMessages={validateMessages}>
            <Form.Item
              // name={['user', 'name']}
              label="商品名称"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input value={this.state.list.name}/>
            </Form.Item>
            <Form.Item
              // name={['user', 'des']}
              label="商品描述"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input value={this.state.list.subtitle} />
            </Form.Item>
            <Form.Item
              // name={['user', 'classify']}
              label="商品一级分类"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Select value={this.state.list.name}>
                <Select.Option >Demo</Select.Option>
                <Select.Option>111</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item label="价格"
              // name={['user', 'price']}
              rules={[
                {
                  required: true,
                },
              ]}>
              <InputNumber value={this.state.list.price}/>
            </Form.Item>
            <Form.Item label="库存"
              // name={['user', 'rest']} 
              rules={[
                {
                  required: true,
                },
              ]}>
              <InputNumber value={this.state.list.stock}/>
            </Form.Item>
            <Form.Item label="上架状态"
              // name={['user', 'status']}
              rules={[
                {
                  required: true,
                },
              ]}>
              <Switch checkedChildren="下架" unCheckedChildren="上架" checked={this.state.list.status===1?true:false}/>
            </Form.Item>
            <Form.Item 
            // name={['user', 'upload']}
             label="Introduction" rules={[
              {
                required: true,
              }]} >
              <Upload
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                listType="picture-card"
                fileList={fileList}
                onPreview={this.handlePreview}
                onChange={this.handleChange}
              >
                {fileList.length >= 8 ? null : uploadButton}
              </Upload>
              <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                <img alt="example" style={{ width: '100%' }} src={previewImage} />
              </Modal>
            </Form.Item>
            <Form.Item 
            // name={['user', 'info']} 
            label="详情" rules={[
              {
                required: true,
              }]}>
              <EditorDemo style={{ "border": "1px solid #ccc" }} />
            </Form.Item>
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
              {/* wrapperCol={{ ...layout.wrapperCol, offset: 8 }} */}
              <Button type="primary" htmlType="submit">
                提交
        </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
  async componentDidMount() {
    await this.setState({
         productId:this.props.match.params.productId
     })
     this.getList()
 }

}