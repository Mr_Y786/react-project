import React from 'react';
import { Breadcrumb, Button, Table, message } from 'antd';
import "./shop.css";
import { http } from "../../http"
import qs from 'qs';
const { Column } = Table;

export class Shop extends React.Component {
    state = {
        data: [],
        params: {
            pageNum: 1,
            pageSize: 10
        },
        value: "",
    }
    getList = () => {
        http("manage/product/list.do", "GET", {}, this.state.params).then(res => {
            this.setState({
                data: res.data.list,
                pagination: {
                    ...this.state.pagination,
                    total: res.data.total
                }
            })
        })
    }
    //分页请求数据
    changePage = async (page) => {
        const { current, pageSize } = page
        await this.setState({
            params: {
                pageNum: current,
                pageSize
            }
        })
        if (this.state.value) {
            this.search()
        } else {
            this.getList();
        }


    }
    //input值
    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }
    //input清空
    reset = () => {
        this.setState({
            value: ""
        })
        this.getList(this.state.params);
    }
    //查询
    search = () => (e) => {
        http("manage/product/search.do", "GET", {}, {
            ...this.state.params,
            productName: this.state.value
        }).then(res => {
            this.setState({
                data: res.data.list,
                pagination: {
                    ...this.state.pagination,
                    total: res.data.total
                }
            })
        })
    }
    //跳转到添加页
    toadd=()=>{
        console.log("111")
        this.props.history.push('/home/shopadd')
    }
    //跳转到编辑页
    toEdit = (obj) =>(e)=> {
        this.props.history.push('/home/shopedit/'+obj.id)
    }
    //跳转到详情页
    toInfo = (obj) =>(e)=> {
        this.props.history.push('/home/shopinfo/'+obj.id)
    }
    //设置上架/下架
    setSendStatus = (obj) => (e) => {
        http("manage/product/set_sale_status.do", "POST", qs.stringify({
            productId: obj.id,
            status: obj.status === 1 ? 2 : 1
        })).then(res => {
            message.success({
                content: res.data,
                duration: 3,
                icon: ""
            });
            this.getList();
        })
    }
    render() {
        return (
            <div>
                {/* 面包屑 */}
                <div className="brand">
                    <Breadcrumb>
                        <Breadcrumb.Item>首页</Breadcrumb.Item>
                        <Breadcrumb.Item>商品 </Breadcrumb.Item>
                        <Breadcrumb.Item>商品列表</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="brand_head">商品列表</div>
                </div>
                <div className="table_list">
                    <div className="table_inp">
                        <div className="inp">关键词： <input type="text" value={this.state.value} onChange={this.handleChange} /></div>
                        <div className="btn">
                            <Button type="primary" onClick={this.search}>搜索</Button>
                            <Button onClick={this.reset}>清空</Button>
                        </div>
                    </div>
                    <Button type="primary" onClick={this.toadd}>新增</Button>
                    <Table dataSource={this.state.data} rowKey={record => record.id} pagination={this.state.pagination} onChange={this.changePage}>
                        <Column title="名称" dataIndex="name" key="name" />
                        <Column title="标题" dataIndex="subtitle" key="subtitle" />
                        <Column title="价格" dataIndex="price" key="price"
                            render={(text) => (
                                <span>￥ {text}</span>
                            )}
                        />
                        <Column
                            title="商品状态"
                            dataIndex="status"
                            key="status"
                            render={(text, record) => (
                                <div>
                                    <div className="goodstatus">
                                        {
                                            text === 1 ? "在售" : "已下架"
                                        }
                                    </div>
                                    <Button onClick={this.setSendStatus(record)}>
                                        {
                                            text === 1 ? "设置下架" : "设置上架"
                                        }
                                    </Button>
                                </div>
                            )}
                        />
                        <Column
                            title="Action"
                            key="action"
                            render={(record) => (
                                <span>
                                    <a style={{ marginRight: 8 }} onClick={this.toInfo(record)}>查看详情</a>
                                    <span>|</span>
                                    <a style={{ marginLeft: 8 }} onClick={this.toEdit(record)}>编辑</a>
                                </span>
                            )}
                        />
                    </Table>
                </div>

            </div>
        )
    }
    componentDidMount() {
        this.getList();
    }
}