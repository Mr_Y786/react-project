import React from 'react';
import { Breadcrumb, PageHeader, Descriptions } from 'antd';
import "./shopinfo.css"
import {http} from "../../http"
export class Shopinfo extends React.Component {
    state={
        productId:"",
        infoList:{}
    }
    getInfo(){
        http("manage/product/detail.do","GET",{},{
            productId:this.state.productId
        }).then(res=>{
            this.setState({
                infoList:res.data
            })
        })
    }
    render() {
        return (
            <div className="shopinfo">
                <div className="brand">
                    <Breadcrumb>
                        <Breadcrumb.Item>首页</Breadcrumb.Item>
                        <Breadcrumb.Item>商品 </Breadcrumb.Item>
                        <Breadcrumb.Item>商品列表</Breadcrumb.Item>
                        <Breadcrumb.Item>商品详情</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="brand_head">商品详情</div>
                </div>
                <div className="info_main">
                    <PageHeader
                        ghost={false}
                        subTitle="商品基本信息"
                    >
                        <Descriptions size="small" column={3}>
        <Descriptions.Item label="名称">{this.state.infoList.name}</Descriptions.Item>
                            <Descriptions.Item label="标题">
                               {this.state.infoList.subtitle}
                            </Descriptions.Item>
                            <Descriptions.Item label="商品分类">{this.state.infoList.name}</Descriptions.Item>
                            <Descriptions.Item label="价格">￥{this.state.infoList.price}</Descriptions.Item>
                            <Descriptions.Item label="上架状态">{
                                this.state.infoList.status===1?'上架':"在售"
                            }</Descriptions.Item>
                            <Descriptions.Item label="库存">{this.state.infoList.stock}</Descriptions.Item>
                        </Descriptions>
                    </PageHeader>
                    <PageHeader
                        ghost={false}
                        subTitle="商品基本信息"
                        className="info_des"
                    >
                        <Descriptions>
                        <Descriptions.Item className="infocon">
                        {this.state.infoList.detail}
                        </Descriptions.Item>
                        </Descriptions>
                    </PageHeader>
                    
                </div>
            </div >
        )
    }
    async componentDidMount() {
       await this.setState({
            productId:this.props.match.params.productId
        })
        this.getInfo()
    }
}