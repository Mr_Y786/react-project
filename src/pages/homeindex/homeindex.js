import React from 'react';
import { http } from "../../http";
import { Row, Col } from 'antd';
import "./homeindex.css"
export class Homeindex extends React.Component {
    state = {
        countList: {}
    }
    getCount = () => {
        http("manage/statistic/base_count.do", "get").then(res => {
            this.setState({
                countList: res.data
            })
        })
    }
    render() {
        return (
            <Row justify="space-around" className="indexcount">
                <Col span={6}>
                    <div className="count_tit">用户总数</div>
                    <div className="count_con">{this.state.countList.userCount}</div>
                </Col>
                <Col span={6}><div className="count_tit">商品总数</div>
                    <div className="count_con">{this.state.countList.productCount}</div></Col>
                <Col span={6}><div className="count_tit">订单数</div>
                    <div className="count_con">{this.state.countList.orderCount}</div></Col>
            </Row>

        )
    }
    componentDidMount() {
        this.getCount()
    }
}