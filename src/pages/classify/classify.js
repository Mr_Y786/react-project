import React from 'react';
import { Breadcrumb, Button, Table, Modal, Form, Input, Select, message } from 'antd';
import { http } from "../../http";
import "./classify.css"
import qs from 'qs';
const { Column } = Table;
const { Option } = Select;
export class Classify extends React.Component {
    state = {
        data: [],
        modal: false,  //弹窗状态
        modal2: false,  //弹窗状态
        params: {
            parentId: "",
            categoryName: ""
        },
        changeParams: {
            categoryId: "",
            categoryName: ""
        }
    }
    //设置弹窗modal状态
    async setModal(status) {
        await this.setState({ modal: status });
    }
    //设置弹窗modal2状态
    async changeModal(status, obj) {
        if (obj) {
            this.setState({
                changeParams: {
                    ...this.state.changeParams,
                    categoryId: obj.id,
                    categoryName: obj.name
                },
                name: obj.name
            });
        }
        await this.setState({ modal2: status });
    }
    //下拉列表
    selectChange = async (value) => {
        await this.setState({
            params: {
                ...this.state.params,
                parentId: value
            }
        })
    }
    //input内容
    onChange = async (event) => {
        await this.setState({
            params: {
                ...this.state.params,
                categoryName: event.target.value
            }
        })
    }
    //input内容
    onChange1 = async (event) => {
        await this.setState({
            changeParams: {
                ...this.state.changeParams,
                categoryName: event.target.value
            }
        })
    }
    //添加分类
    addClassify = () => {
        http("manage/category/add_category.do", "POST", qs.stringify({
            ...this.state.params
        })).then(async res => {
            message.success({
                content: res.data,
                duration: 3,
                icon: ""
            });
            await this.setState({
                modal: false
            })
            this.getList()
        })

    }
    //修改分类
    changeClassify = () => {
        http("manage/category/set_category_name.do", "POST", qs.stringify({
            ...this.state.changeParams
        })).then(async res => {
            message.success({
                content: res.data,
                duration: 3,
                icon: ""
            });
            await this.setState({
                modal2: false
            })
            this.getList()
        })
        // console.log(this.state.params.categoryName)
    }
    //跳转到子列表
    toSubClassify = (id) => (e) => {
        this.props.history.push(`/home/subclassify/${id}`)
    }
    //获取列表
    getList = () => {
        http("manage/category/get_category.do", "GET", {}, { categoryId: 0 }).then(res => {
            // console.log(res)
            this.setState({
                data: res.data
            })
        })
    }
    render() {
        const {categoryName}=this.state.changeParams
        return (
            <div>
                {/* 面包屑 */}
                <div className="brand">
                    <Breadcrumb>
                        <Breadcrumb.Item>首页</Breadcrumb.Item>
                        <Breadcrumb.Item>一级品类管理 </Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="brand_head">一级品类管理</div>
                </div>
                <div className="table_list">
                    <Button type="primary" onClick={() => this.setModal(true)}>新增</Button>
                    <Table dataSource={this.state.data} rowKey={record => record.id} pagination={this.state.pagination} onChange={this.changePage}>
                        <Column title="名称" dataIndex="name" key="name" />
                        <Column
                            title="操作"
                            key="action"
                            render={(record) => (
                                <span>
                                    <a style={{ marginRight: 8 }} onClick={() => this.changeModal(true, record)}>修改</a>
                                    <span>|</span>
                                    <a style={{ marginLeft: 8 }} onClick={this.toSubClassify(record.id)}>查看子分类</a>
                                </span>
                            )}
                        />
                    </Table>
                </div>
                {/* 新增dialog */}
                <Modal
                    title="新增品类"
                    centered
                    visible={this.state.modal}
                    onOk={this.addClassify}
                    onCancel={() => this.setModal(false)}
                >
                    <Form>
                        <Form.Item name="" label="父级品类" rules={[{ required: true, message: '请选择品类' }]}>
                            <Select
                                placeholder="请选择父级品类"
                                onChange={this.selectChange}
                                defaultValue=''
                            >
                                <Option value="0">根品类</Option>
                                {this.state.data.map(item =>
                                    <Option value={item.id} key={item.id}>{item.name}</Option>
                                )}
                            </Select>
                        </Form.Item>
                        <Form.Item name="name" label="名称" rules={[{ required: true, message: '请输入名称' }]}>
                            <Input value={this.state.params.categoryName} onChange={this.onChange} />
                        </Form.Item>
                    </Form>
                </Modal>
                {/* 修改品类名称 */}
                <Modal
                    title={`修改品类${this.state.name}名称`}
                    centered
                    visible={this.state.modal2}
                    onOk={this.changeClassify}
                    onCancel={() => this.changeModal(false)}
                >
                    <Form>

                        <Form.Item name="name" label="名称"
                            rules={[{ required: true, message: '请输入名称' }]}
                        >
                            <Input value={categoryName} onChange={this.onChange1} />
                        </Form.Item>
                    </Form>
                </Modal>
            </div >
        )
    }
    componentDidMount() {
        this.getList();
    }
}