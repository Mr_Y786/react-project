import React from 'react';
import { Breadcrumb, Table } from 'antd';
import { http } from "../../http"
const { Column } = Table;
export class User extends React.Component {
    state = {
        data: [],
        params: {
            pageNum: 1,
            pageSize: 10
        },
        value: ""
    }
    getList = (params) => {

        http("manage/user/list.do", "GET", {}, params).then(res => {
            this.setState({
                data: res.data.list,
                pagination: {
                    ...this.state.pagination,
                    total: res.data.total
                }
            })
        })
    }
    //分页请求数据
    changePage = (page) => {
        const { current, pageSize } = page
        let params = {
            pageNum: current,
            pageSize
        }
        this.getList(params);
    }
    
    render() {
        return (
            <div>
                {/* 面包屑 */}
                <div className="brand">
                    <Breadcrumb>
                        <Breadcrumb.Item>首页</Breadcrumb.Item>
                        <Breadcrumb.Item>用户列表</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="brand_head">用户列表</div>
                </div>
                <div className="table_list">
                    <Table dataSource={this.state.data} rowKey={record => record.id} pagination={this.state.pagination} onChange={this.changePage}>
                        <Column title="帐号" dataIndex="username" key="username" />
                        <Column title="电话" dataIndex="phone" key="phone" />
                        <Column title="邮箱" dataIndex="email" key="email"
                           
                        />
                        <Column
                            title="注册时间"
                            dataIndex="updateTime"
                            key="updateTime"
                            render={(text) => (
                                <span>{text}</span>
                            )}
                        />
                    </Table>
                </div>

            </div>
        )
    }
    componentDidMount() {
        this.getList(this.state.params);
    }
}