import React from 'react';
import "./home.css"
import { Layout, Menu, Avatar, Popover } from 'antd';
import { Route, Link,Switch} from "react-router-dom";
import { Homeindex } from "../homeindex/homeindex";

import { Shop } from "../shop/shop";
import { ShopEdit } from "../shop/shopedit";
import { Shopadd } from "../shop/shopadd";
import {Shopinfo} from "../shop/shopinfo"
import {Classify} from "../classify/classify";
import {SubClassify} from "../classify/subClassify"
import {Order} from "../order/order";
import {Orderinfo} from "../order/orderInfo"
import {User} from "../user/user"
import {
    DesktopOutlined,
    PieChartOutlined,
    FileOutlined,
    TeamOutlined,
    UserOutlined,
} from '@ant-design/icons';

const { Header, Content, Sider,Footer } = Layout;
const { SubMenu } = Menu;

export class Home extends React.Component {
    rootSubmenuKeys = ['sub1', 'sub2','sub3','sub4', 'sub5'];
    state = {
        collapsed: false,
        openKeys: ['sub1'],
    };
    //点击菜单其他收起
    onOpenChange = openKeys => {
        const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
        if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
          this.setState({ openKeys });
        } else {
          this.setState({
            openKeys: latestOpenKey ? [latestOpenKey] : [],
          });
        }
      };
      //点击最下边箭头左右收缩
    onCollapse = collapsed => {
        this.setState({ collapsed });
    };
    handleClick = e => {
        console.log('click ', e);
      };
    render() {
        return (
            <div>
                <Layout style={{ minHeight: '100vh' }}>
                    <Sider collapsible collapsed={this.state.collapsed} >
                        <div className="logo" />
                        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" openKeys={this.state.openKeys} onOpenChange={this.onOpenChange}>
                            <SubMenu
                                key="sub1"
                                title={
                                    <span>
                                        <UserOutlined />
                                        <span>仪表盘</span>
                                    </span>
                                }
                            >
                                <Menu.Item key="1">
                                    <PieChartOutlined />
                                    <Link to="/home/index">首页</Link>
                                </Menu.Item>
                            </SubMenu>
                            <SubMenu
                                key="sub2"
                                title={
                                    <span>
                                        <UserOutlined />
                                        <span>商品</span>
                                    </span>
                                }
                            >
                                <Menu.Item key="2">
                                    <DesktopOutlined />
                                    <Link to="/home/shop">商品管理</Link>
                                </Menu.Item>
                            </SubMenu>
                            <SubMenu
                                key="sub3"
                                title={
                                    <span>
                                        <TeamOutlined />     
                                        <span>品类</span>
                                    </span>
                                }
                            >
                                <Menu.Item key="3">
                                    <DesktopOutlined />
                                    <Link to="/home/classify">品类管理</Link>
                                </Menu.Item>
                            </SubMenu>
                            <SubMenu
                                key="sub4"
                                title={
                                    <span>
                                        <UserOutlined />
                                        <span>订单</span>
                                    </span>
                                }
                            >
                                <Menu.Item key="4">
                                    <FileOutlined />
                                    <Link to="/home/order">订单管理</Link>
                                </Menu.Item>
                            </SubMenu>
                            <SubMenu
                                key="sub5"
                                title={
                                    <span>
                                        <UserOutlined />
                                        <span>用户</span>
                                    </span>
                                }
                            >
                                <Menu.Item key="5">
                                    <DesktopOutlined />
                                    <Link to="/home/user">用户管理</Link>
                                </Menu.Item>
                            </SubMenu>
                        </Menu>
                    </Sider>
                    <Layout className="site-layout">
                        <Header className="site-layout-background shadow" style={{ padding: 0 }} >
                            <div className="head-reset">
                                <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                <Popover content={<Link to="/"> 退出登录 </Link>}>admin </Popover>
                            </div>
                        </Header>
                        <Content style={{ margin: '10px 16px' }}>
                            {/* <Homeindex></Homeindex> */}
                            
                                <Switch>
                                    <Route path="/home/index" component={Homeindex}></Route>
                                    <Route path="/home/shop" component={Shop} ></Route>
                                    <Route path="/home/shopadd" component={Shopadd}></Route>
                                    <Route path="/home/shopedit/:productId" component={ShopEdit}></Route>
                                    <Route path="/home/shopinfo/:productId" component={Shopinfo}></Route>
                                    <Route path="/home/classify" component={Classify}></Route>
                                    <Route path="/home/subclassify/:id" component={SubClassify}></Route>
                                    <Route path="/home/order" component={Order} ></Route>
                                    <Route path="/home/orderinfo/:orderNo" component={Orderinfo}></Route>
                                    <Route path="/home/user" component={User}></Route>
                                </Switch>
                        </Content>
                        <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
                    </Layout>
                </Layout>
            </div>
        );
    }
}
