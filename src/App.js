import React from 'react';
import "./lib/reset.css"
import './App.css';
import 'antd/dist/antd.css';
import { Login } from "./pages/login/login";
import { Home } from "./pages/home/home";
import {Error} from "./pages/error/error"

import { Route, Switch, Redirect } from "react-router-dom";
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Redirect exact from="/" to="/login"></Redirect>
          <Route path='/login' component={Login}></Route>
          <Route  path='/home' component={Home}></Route>
          <Route component={Error}></Route>
        </Switch>
      </div>
    );
  }
}


export default App;
