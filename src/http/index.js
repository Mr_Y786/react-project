import axios from "axios"
// import {message} from 'antd'
const instance = axios.create({
    // baseURL: "http://admin.raz-kid.cn/api/"
})

export function http(url, method, data, params) {
    return new Promise((resolve, reject) => {
        instance({
            method,
            url:"/api/"+url,
            data,
            params
        }).then(res => {
            if ((res.status >= 200 && res.status < 300) || res.status === 304) {
                // if (res.data.status ===0) {
                //      message.success({
                //         content:res.data.msg,
                //         duration:3,
                //         icon:""
                //     });
                    resolve(res.data)
                     
                // } else {
                    
                //     message.error({
                //         content:res.data.msg,
                //         duration:3,
                //         icon:""
                //     });
                //     reject(res)
                // }
            } else {
               reject(res)
            }
        }).catch(err => {
            
            reject(err)
        })
    })
}
